#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

// Your global variables here

// Defination of pot and pin variables
#define LCD_Port         PORTD	// Define LCD Port (PORTA, PORTB, PORTC, PORTD)
#define LCD_DPin         DDRD	// Define 4-Bit Pins (PD4-PD7 at PORT D)
#define LCD_DATA0_PIN    PD4    // < pin for 4bit data bit 0  
#define LCD_DATA1_PIN    PD5    // < pin for 4bit data bit 1  
#define LCD_DATA2_PIN    PD6    // < pin for 4bit data bit 2  
#define LCD_DATA3_PIN    PD7    // < pin for 4bit data bit 3  
#define RSPIN 			 PD3	// RS Pin
#define ENPIN            PD2 	// E Pin
int runtime;			 //Timer for LCD
int switch1[3]={0,0,0}; 
int history[3]={0,0,0}; 
int color=1;		

// LCD initialization function with the minimum commands list
void LCD_Init (void)
{
    // Data and control pins as outputs
    LCD_DPin |= (1<<LCD_DATA0_PIN)|(1<<LCD_DATA1_PIN)|(1<<LCD_DATA2_PIN)|(1<<LCD_DATA3_PIN)|(1<<RSPIN)|(1<<ENPIN);		//Control LCD Pins (D4-D7)
	_delay_ms(16);		//Wait more when 15 ms after start
  
  
	LCD_Action(0x02);	    // Returns the cursor to the home position (Address 0). Returns display to its original state if it was shifted. 
	LCD_Action(0x28);       // Data sent or received in 4 bit lengths (DB7-DB4)
	LCD_Action(0x0C);       // Display on, cursor off
	LCD_Action(0x06);       // Increment cursor (shift cursor to right)
	LCD_Action(0x01);       // Clean LCD
	_delay_ms(5);
}

// Function to send commands and data to the LCD
void LCD_Action( unsigned char cmnd )
{
	LCD_Port = (LCD_Port & 0x0F) | (cmnd & 0xF0);
	LCD_Port &= ~ (1<<RSPIN);
	LCD_Port |= (1<<ENPIN);
	_delay_us(1);
	LCD_Port &= ~ (1<<ENPIN);
	_delay_us(200);
	LCD_Port = (LCD_Port & 0x0F) | (cmnd << 4);
	LCD_Port |= (1<<ENPIN);
	_delay_us(1);
	LCD_Port &= ~ (1<<ENPIN);
	_delay_ms(2);
}

// Clear whole LCD
void LCD_Clear()
{
	LCD_Action (0x01);		//Clear LCD
	_delay_ms(2);			//Wait to clean LCD
	LCD_Action (0x80);		//Move to Position Line 1, Position 1
}

// Prints string of chars 
void LCD_Print (char *str)
{
	int i;
	for(i=0; str[i]!=0; i++)
	{
		LCD_Port = (LCD_Port & 0x0F) | (str[i] & 0xF0);
		LCD_Port |= (1<<RSPIN);
		LCD_Port|= (1<<ENPIN);
		_delay_us(1);
		LCD_Port &= ~ (1<<ENPIN);
		_delay_us(200);
		LCD_Port = (LCD_Port & 0x0F) | (str[i] << 4);
		LCD_Port |= (1<<ENPIN);
		_delay_us(1);
		LCD_Port &= ~ (1<<ENPIN);
		_delay_ms(2);
	}
}

// Prints string of chars to the specific location
// row - the row 0 or 1;
// pos - the column from 0 till 16
void LCD_Printpos (char row, char pos, char *str)
{
	if (row == 0 && pos<16)
	LCD_Action((pos & 0x0F)|0x80);
	else if (row == 1 && pos<16)
	LCD_Action((pos & 0x0F)|0xC0);
	LCD_Print(str);
}

// Your functions here

int main()
{
  	DDRB=0b00111000;
 	PCICR|=(1<<PCIE0);
  	PCMSK0|=(1<<PCINT0)|(1<<PCINT1)|(1<<PCINT2);
 	char vilniustech[17]={"VilniusTech"};
  	char vilniusmenu[17]={"VilniusTech Menu"};
  	char led_select[3][17]={"LED Red: ","LED Green: ","LED Yellow: "};
  	char led_status[2][17]={"OFF","ON"};
  	int letter_pos = 0;
  	LCD_Init();
  	
  	while(1){
   	LCD_Printpos(0,0, vilniustech);
  }
  	
	/*while(1) {
      LCD_Clear();
	  LCD_Printpos(1,letter_pos, vilniustech);
      letter_pos = (letter_pos-1)%16;
      _delay_ms(300);
      }
      */
      
  while(1){
      sei();
      LCD_Printpos(0,0,vilniusmenu);
      LCD_Printpos(1,0,led_select[color]);
      LCD_Printpos(1,13,led_status[switch1[color]]);
    _delay_ms(250);
	}
}
      

ISR(PCINT0_vect){
  if (!(PINB & (1<<PB1))) { 
    color+=1;
  	if (color==3) color=0;}
  if (!(PINB & (1<<PB0))) { 
    color-=1;
  	if (color==-1) color=2;}
  if (!(PINB & (1<<PB2))) {
    if (switch1[color]==0) switch1[color]=1;
    else switch1[color]=0;}
  for (int i = 0; i<3; i++)
  {
    if (history[i]!=switch1[i])
    {
       if (i==0) PORTB^= (1 << 3);
       else if (i==1) PORTB^= (1 << 4);
       else PORTB^= (1 << 5);
       history[i] = switch1[i];
    }
  }
  LCD_Clear();
}